public final class ContaCorrente extends Conta {

	public ContaCorrente(int agencia, int conta, String titular, int limite, int saldo) {
		super(agencia, conta, titular, limite, saldo);
	}

	@Override
	public void sacar(int valor) {
		
		if(valor < this.saldo) {
			
			this.saldo -= valor;
			System.out.println("Retirado " + valor + "reais do seu saldo.");
		}
		
		else if(valor < this.limite) {
			
			this.limite -= valor;
			System.out.println("Saldo insuficiente. Retirado " + valor + "reais do seu limite de cr�dito.");
			
		} else {
			
			System.out.println("Valor excede o seu limite. N�o foi possivel efetuar a opera��o.");
			
		}
	}
	
	
	
	

}
