public abstract class Conta {

	public int agencia;
	public int conta;
	public String titular;
	public int limite;
	public int saldo;
	public static final int VALORLIMITE = 500;
	
	public Conta(int agencia, int conta, String titular, int limite, int saldo) {
		super();
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
		this.limite = limite;
		this.saldo = saldo;
	}
	
	public Conta(int agencia, int conta, String titular, int saldo) {
		super();
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
		this.saldo = saldo;
	}
	
	
	public abstract void sacar(int valor);
	
	
	public final void depositar(int valor) {
		
		this.saldo -= valor;	
		
	}
	
	public void setValorLimite(int valor) {
		
		if(valor <= VALORLIMITE) {
			
			this.limite = valor;
			
		} else {
			
			System.out.println("O valor excede o limite de credito de 5000.");
			
		}
		
	}
	
	
	
	
	
	
	
	
}
