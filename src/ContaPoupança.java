public final class ContaPoupança extends Conta {

	public ContaPoupança(int agencia, int conta, String titular, int saldo) {
		super(agencia, conta, titular, saldo);
	}

	@Override
	public void sacar(int valor) {
				
		if(valor < this.saldo) {
					
			this.saldo -= valor;
			System.out.println("Retirado " + valor + "reais do seu saldo.");
					
		}  else {
			
			System.out.println("Valor excede o seu saldo. Nao foi possivel efetuar a operação");
			
			
		}
		
		

	}
	
	

}
